export default {
  data () {
    return {
      notify: {
        color: 'default',
        label: '',
        activated: false
      }
    }
  },
  methods: {
    setPositiveNotify (label) {
      this.notify.label = label
      this.notify.color = 'success',
      this.notify.activated = true
    },
    setNegativeNotify (label) {
      this.notify.label = label
      this.notify.color = 'error',
      this.notify.activated = true
    },
    resetNotify () {
      this.notify.label = ''
      this.notify.color = 'default',
      this.notify.activated = false
    }
  },
}