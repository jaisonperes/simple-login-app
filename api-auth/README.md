# API de Usuários

# Instalando e configurando

A API foi feita visando o uso de Python3.

>$ pip install -r requirements.txt

*Obs.*

Recomenda-se fortemente que se tenha um ambiente virtual dedicado por aplicações, para evitar comprometer a estabildiade do seu sistema operacional. Utilize o [VirtualEnvWrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) ou um container [Docker](https://hub.docker.com/_/python/).


- Crie um arquivp `.env` no mesmo diretório do arquivo [api.py](api.py) com o seguinte formato:

```
MONGO_USER=admin
MONGO_PASS=admin
MONGO_HOST=localhost
MONGO_PORT=27017
MONGO_DBNAME=reales_rent
```

# Executando

> $ python3 api.py

A API deve estar disponivel na porta 8000.


# Executando os testes

> $ coverage run --source=. -m unittest discover -s test

# Executando o linter

> $ pylint *.py

# Servindo

Sirva com:

> gunicorn -b :8000  -w 8 -k tornado --log-level warning --log-file error_logs.log   wsgi
